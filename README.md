# MSSQL docker-compose Linux
- This is a simple docker-compose file to set up Microsoft SQL Sever in Linux 

#### PreRequest
- `Docker` [installation](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)
- `dokcer-compose` [installation](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04)


#### Getting started
- Clone this repo
- ``
    $ docker-compose up -d --build
    ``

#### Validate if MSSQL is running [either one]
1. Check if port `1433` is running
2. Enter your dokcer container and run
    ``` 
    $ /opt/mssql-tools/bin/sqlcmd -S localhost,1433 -U sa -P "$MSSQL_SA_PASSWORD"  
    > 1 ==> If you see this, MSSQL is up and running
    ```
3. DBeaver or your favourite DB inspector 
    - Host: Your Server IP or localhost
    - Port: `1433`
    - Username: `sa` (default user)
    - Password: `P@ssw0rd1234` (see `MSSQL_SA_PASSWORD` in docker-compose.yml)

